class ApplicationMailer < ActionMailer::Base
  default from: 'srtatmg@gmail.com'
  layout 'mailer'
end
